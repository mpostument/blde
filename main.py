def main():
    book_path = "books/frankenstein.txt"
    text = read_file(book_path)
    num_words = get_num_words(text)
    print(num_words)
    chars_dict = get_chars_dict(text)
    report_data = chars_dict_to_list(chars_dict)

    for item in report_data:
        if not item["symbol"].isalpha():
            continue
        print(f"The '{item['symbol']}' character was found {item['count']} times")


def get_num_words(text):
    words = text.split()
    return len(words)


def get_chars_dict(text):
    chars = {}
    for c in text:
        lowered = c.lower()
        if lowered in chars:
            chars[lowered] += 1
        else:
            chars[lowered] = 1
    return chars


def read_file(path):
    with open(path) as f:
        return f.read()


def chars_dict_to_list(num_chars_dict):
    sorted_list = []
    for ch in num_chars_dict:
        sorted_list.append({"symbol": ch, "count": num_chars_dict[ch]})
    sorted_list.sort(reverse=True, key=lambda x: x["count"])
    return sorted_list


main()
